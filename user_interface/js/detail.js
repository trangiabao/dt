const showDetail = function () {
  const id = window.location.search.split("=")[1];
  console.log(id);

  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${id}`,
    method: "GET",
    data: null,
  })
    .then(function (res) {
      document.getElementById(
        "txtName"
      ).innerHTML = res.data.name.toUpperCase();

      const html = `
        <img  src="${res.data.image}" alt="">
        <div class="detail_text">
            <p>${res.data.description}</p>
            <p>Giá: <span>${res.data.price}</span></p>
            <p>Số lượng còn lại: ${res.data.inventory}</p>
            <p>Type:  <span>#${res.data.type}</span></p>
            <p>Rate: ${rate(+res.data.rating)}</p>
            <button class="btn btn-danger" onclick="addToCart(${res.data.id})">Add to cart <i class="fa fa-shopping-cart"></i></button>
        </div>
        `;

      document.getElementById("detail-content").innerHTML = html;
    })
    .catch(function (err) {
      console.log(err);
    });
};
const rate = function (number) {
            return `
            <i class="fa fa-star text-warning"></i>
            <i class="fa fa-star text-warning"></i>
            <i class="fa fa-star text-warning"></i>
            <i class="fa fa-star text-warning"></i>
            <i class="fa fa-star text-secondary"></i>
            `;
}
showDetail();
