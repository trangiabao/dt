function phoneDisplay() {
  document.getElementById("phone").style.transform = "translateX(0)";
  var overplay = document.getElementsByClassName("overplay");
  overplay[0].style.display = "block";
}

function phoneDisplayNone() {
  document.getElementById("phone").style.transform = "translateX(100%)";
  var overplay = document.getElementsByClassName("overplay");
  overplay[0].style.display = "none";
}

var list = [];
let cartList = [];
var fetchData = function () {
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
    data: null,
  })
    .then(function (res) {
      var place = document.getElementById("list-product");
      addList(res.data);
      renderProduct(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
};

const addList = (data) => {
  data.forEach(function (item) {
    list.push(
      new Phone(
        item.id,
        item.name,
        item.image,
        item.description,
        item.price,
        item.inventory,
        item.rating,
        item.type
      )
    );
  });
};

const search = function () {
  var input = document.getElementById("txtSearch").value.trim();
  var search = document.getElementById("search-content");
  var html = "";
  if (input != "") {
    search.style.display = "block";
  }
  for (i = 0; i < list.length; i++) {
    if (list[i].name.toLowerCase().indexOf(input.toLowerCase()) != -1) {
      html += `
      <div class="" onclick="viewDetail(${list[i].id})">
        <img src="${list[i].image}" alt="">
        <p>${list[i].name}</p>
      </div>
      `;
    }
  }
  if (input === "") {
    html = "";
  }
  search.innerHTML = html;
};

const renderProduct = function (data) {
  var html = "";
  for (var i = 0; i < data.length; i++) {
    html += `
            <div class="item-wrapper col-sm-6 col-md-4" onclick="viewDetail(${
              data[i].id
            })">
                <img src="${data[i].image}" class="img-fluid" alt="">
                <h2>${data[i].name.toUpperCase()}</h2>
                <p>${data[i].description}</p>
                <span>${data[i].price}$</span>
            </div>
        `;
  }
  document.getElementById("list-product").innerHTML = html;
};

const viewDetail = function (id) {
  window.location.assign(`detail.html?id=${id}`);
};

fetchData();

const sort = () => {
  list.sort(function (a, b) {
    var nameA = a.name.toUpperCase();
    var nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }

    // name trùng nhau
    return 0;
  });
  renderProduct(list);
};

const sortReverse = () => {
  sort();
  list = list.reverse();
  renderProduct(list);
};

const sortDisplay = () => {
  const sortButton = document.getElementById("sort").value;

  if (sortButton === "A-Z") {
    return sort();
  }
  if (sortButton === "Z-A") {
    return sortReverse();
  }
};

const checkCartItem = (id) => {
  for (let i = 0; i < cartList.length; i++) {
    if (cartList[i].Phone.id === id + "") {
      return i;
    }
  }
  return -1;
};

const addToCart = (id) => {
  if (checkCartItem(id) == -1) {
    let cart = new Cart(list[findByID(id)], 1);
    cartList.push(cart);
  } else {
    cartList[checkCartItem(id)].quantity += 1;
  }
  storeToCart(cartList);
  cartRender();
  swal("Thêm thành công!", "Kiểm tra tại giỏ hàng của bạn", "success");
};

const findByID = (id) => {
  for (i = 0; i < list.length; i++) {
    if (list[i].id + "" === id + "") {
      return i;
    }
  }
};

const storeToCart = (cart) => {
  localStorage.setItem("mycart", JSON.stringify(cart));
  cartNotification();
};

const fetchDataCart = () => {
  const dataCart = localStorage.getItem("mycart");
  if (dataCart) {
    cartList = JSON.parse(dataCart);
  } else {
    return;
  }
};

const cartNotification = () => {
  if (cartList.length <= 0) {
    document.getElementById("cart-text").style.display = "none";
    document.getElementById("cart-content").style.display = "none";
  } else {
    document.getElementById("cart-text").style.display = "flex";
    document.getElementById("cart-text").innerHTML = `${cartList.length}`;
    document.getElementById("cart-content").innerHTML;
  }
};

const cartRender = () => {
  let html = "";
  if (cartList) {
    cartList.forEach(function (item) {
      html += `
    <div class="item">
        <img
          src="${item.Phone.image}"
        />
        <span>${item.Phone.name.toUpperCase()}</span>
        <div>${item.quantity}</div>
        <span>${item.Phone.price}$</span>
        <button class="btn btn-danger" onclick="deleteItemCart('${
          item.Phone.id
        }')"><i class="fa fa-times"></i></button>
    </div>
      `;
    });
  }

  if (html === "") {
    html = "Bạn chưa thêm sản phẩm nào.";
  } else {
    html += `
    <hr>
      <div id="money">
        <span>tổng tiền: </span>
        <span>${countSumOfMoney()}$</span>
      </div>
      <div id="pay" onclick="clearCartList()">
        <button class="btn btn-info">Thanh toán</button>
      </div>
    `;
  }

  document.getElementById("cart-content").innerHTML = html;
};

const clearCartList = () => {
  cartList = [];
  storeToCart(cartList);
  cartNotification();
  cartRender();
  swal(
    "Thanh toán thành công!",
    "Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi",
    "success"
  );
};

const countSumOfMoney = () => {
  let sum = 0;
  cartList.forEach(function (item) {
    sum += item.Phone.price * item.quantity;
  });
  return sum;
};

const cartDisplay = () => {
  if (document.getElementById("cart-content").style.display == "none") {
    document.getElementById("cart-content").style.display = "block";
  } else {
    document.getElementById("cart-content").style.display = "none";
  }
};

const findCartByID = (id) => {
  cartList.forEach(function (item, index) {
    if (id === item.Phone.id) return index;
  });
};

const deleteItemCart = (id) => {
  cartList.splice(findCartByID(id), 1);
  storeToCart(cartList);
  cartNotification();
  cartRender();
  swal("Xóa thành công!", "Giỏ hàng của bạn đã được cập nhât", "success");
};

fetchDataCart();
cartNotification();
cartRender();
